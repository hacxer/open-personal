package com.cloud.app.web;

import com.cloud.app.service.AppService;
import com.cloud.platform.BaseHandler;

public class AppBean extends BaseHandler {
	
	private AppService appService;
	private String courseInfo;
	
	/**
	 * get app course info
	 * 
	 * @return
	 */
	public String getCourseInfo() {
		if(courseInfo == null) {
			try {
				courseInfo = appService.getCourseInfo(getLoginUserId());
				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		return courseInfo;
	}

	/**
	 * save course info
	 */
	public void saveCourse() {
		try {
			appService.saveCourse(getParameter("courseInfo"), getLoginUserId());
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public AppService getAppService() {
		return appService;
	}

	public void setAppService(AppService appService) {
		this.appService = appService;
	}
	
	public void setCourseInfo(String courseInfo) {
		this.courseInfo = courseInfo;
	}

}
