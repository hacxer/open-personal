package com.cloud.goal.model;

import java.util.Date;

public class HabitDaily {

	private String id;
	private String uid;
	private String habitId;
	private Date createTime;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getUid() {
		return uid;
	}
	
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public String getHabitId() {
		return habitId;
	}
	
	public void setHabitId(String habitId) {
		this.habitId = habitId;
	}
	
	public Date getCreateTime() {
		return createTime;
	}
	
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
