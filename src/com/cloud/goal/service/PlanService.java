package com.cloud.goal.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cloud.goal.model.Plan;
import com.cloud.platform.IDao;
import com.cloud.platform.util.Constants;
import com.cloud.platform.util.StringUtil;

@Component
public class PlanService {

	@Autowired
	private IDao dao;
	
	/**
	 * remove plan
	 * 
	 * @param planId
	 */
	public void removePlan(String planId) {
		if(StringUtil.isNullOrEmpty(planId)) {
			return;
		}
		
		dao.removeById(Plan.class, planId);
	}
	
	/**
	 * search goal plans
	 * 
	 * @param goalId
	 * @return
	 */
	public List<Plan> searchPlans(String goalId) {
		String hql = "from Plan where goalId = ? order by createTime";
		
		return dao.getAllByHql(hql, goalId);
	}
	
	/**
	 * get plan by plan id
	 * 
	 * @param planId
	 * @return
	 */
	public Plan getPlan(String planId) {
		if(StringUtil.isNullOrEmpty(planId)) {
			return new Plan();
		}
		
		return (Plan) dao.getObject(Plan.class, planId);
	}
	
	/**
	 * save goal plan
	 * 
	 * @param plan
	 */
	public void savePlan(Plan plan) {
		if(plan == null) {
			return;
		}
		
		if(StringUtil.isNullOrEmpty(plan.getId())) {
			plan.setId(Constants.getID());
			plan.setCreateTime(new Date());
		}
		
		plan.setModifyTime(new Date());
		
		dao.saveObject(plan);
	}
}
