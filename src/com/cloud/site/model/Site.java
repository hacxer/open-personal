package com.cloud.site.model;

import java.util.Date;

public class Site {

	private String id;
	private int sn;
	private String uid;
	private String catagoryId;
	private String name;
	private String url;
	private Date createTime;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public int getSn() {
		return sn;
	}
	
	public void setSn(int sn) {
		this.sn = sn;
	}
	
	public String getUid() {
		return uid;
	}
	
	public void setUid(String uid) {
		this.uid = uid;
	}
	
	public String getCatagoryId() {
		return catagoryId;
	}
	
	public void setCatagoryId(String catagoryId) {
		this.catagoryId = catagoryId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
