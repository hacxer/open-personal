package com.cloud.knowledge.web;

import java.util.List;

import com.cloud.knowledge.model.Know;
import com.cloud.knowledge.service.KnowService;
import com.cloud.platform.BaseHandler;
import com.cloud.platform.util.Constants;
import com.cloud.platform.util.StringUtil;

public class KnowBean extends BaseHandler {

	private KnowService knowService;
	
	private Know know;
	private List<Know> knows;
	
	public void save() {
		try {
			String userId = getLoginUserId();
			String knowledgeId = getParameter("knowledgeId");
			String info = getParameter("knowInfo");
			
			// set task id for return to page
			knowService.saveKnowInfo(userId, knowledgeId, info);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<Know> getKnows() {
		if(knows == null) {
			String isValid = getParameter("isValid");
			
			if(StringUtil.isNullOrEmpty(isValid)) {
				isValid = Constants.VALID_YES;
			}
			
			knows = knowService.searchKnows(getParameter("knowledgeId"), isValid);
		}
		return knows;
	}

	public void saveKnow() {
		String knowId = getParameter("knowId");
		
		String snid = getParameter("snid");
		String parentId = getParameter("parentId");
		String knowledgeId = getParameter("knowledgeId");
		
		if(!StringUtil.isNullOrEmpty(knowId)) {
			Know k = knowService.getKnow(knowId);
			
			k.setName(know.getName());
			k.setContent(know.getContent());
			
			// reset know for edit
			know = k;
		}
		// set parent id only create a new know
		else {
			know.setParentId(parentId);
		}
		
		knowService.saveKnow(know, getLoginUserId(), knowledgeId, snid);
	}

	public Know getKnow() {
		if(know == null) {
			know = knowService.getKnow(getParameter("knowId"));
		}
		return know;
	}
	
	/**
	 * =======================  getters and setters  =======================
	 */
	public void setKnows(List<Know> knows) {
		this.knows = knows;
	}

	public void setKnow(Know know) {
		this.know = know;
	}

	public KnowService getKnowService() {
		return knowService;
	}

	public void setKnowService(KnowService knowService) {
		this.knowService = knowService;
	}
}
