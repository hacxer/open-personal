<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	
	<style>
		table {margin-left: 20px;}
		tr {height: 40px;}
		td {border-bottom: 1px dotted #ccc;}
	</style>
</head>

<body>
	<div class="menu-title">
		<a href="#" onclick="addHabit();return false;">新增习惯</a>
		<a href="habitsTotal.jsf">习惯统计</a>
	</div>
	
	<table>
		<a4j:repeat value="#{habitBean.habits}" var="item">
		<tr id="<h:outputText value="#{item.id}" />">
			<td width="60px"><h:outputText value="#{item.sn}" /></td>
			<td width="500px"><h:outputText value="#{item.name}" /></td>
			<td width="60px"><input type="checkbox" /></td>
		</tr>
		</a4j:repeat>
		
		<tr>
			<td colspan="3" align="center" style="border-bottom: 0;"><input onclick="saveDailyHabit();" type="button" class="button" value="提交今日习惯保持" /></td>
		</tr>
	</table>
	
	<h:form id="operate">
		<h:inputHidden id="dailyInfo" value="#{habitBean.dailyInfo}" />
		<a4j:commandLink id="save" action="#{habitBean.saveDailyHabit}" oncomplete="alert('提交成功');" /> 
	</h:form>
	
	<script>
		init();
		
		function init() {
			var info = $("#operate\\:dailyInfo").val();
			if(!info)  return;
			
			var ids = info.split(",");
			
			for(var i in ids) {
				$(":checkbox", "#" + ids[i]).attr("checked", true);
			}
		}
	
		function addHabit() {
			buildPage("hbtFrm", basePath + "pages/goal/habitAdd.jsf");
		}
		
		function refresh() {
			location.href = location.href;
		}
		
		function saveDailyHabit() {
			var ids = "";
			
			$("input:checkbox:checked").each(function() {
				if(ids)  ids += ",";
				ids += $(this).closest("tr").attr("id");
			});
			
			$("#operate\\:dailyInfo").val(ids);
			
			$("#operate\\:save").click();
		}
	</script>
</body>
</html>
</f:view>