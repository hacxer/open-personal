<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/cloud.ui.table.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/omui/themes/apusic/om-all.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery.ui.core.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery.ui.widget.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery.ui.mouse.js" />"></script>
	
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/cloud.ui.table.js" />"></script>
	
	<script type="text/javascript" src="<c:url value="/omui/ui/om-core.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-menu.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-mouse.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-draggable.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-position.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-messagebox.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-button.js" />"></script>
</head>

<body>
	<div class="menu-title">
		<a href="#" onclick="addGoal();return false;">新增目标</a>
	</div>
	
	<h:form id="operate">
		<table id="goalTab" class="list-table" cellpadding="0" cellspacing="0">
			<tr>
				<th width="50px">序号</th>
				<th width="400px">名称</th>
				<th width="100px">起始日期</th>
				<th width="100px">结束日期</th>
				<th width="100px">目标类型</th>
				<th width="100px">优先级</th>
				<th width="150px">创建时间</th>
				<th width="150px">更新时间</th>
			</tr>
			
			<a4j:repeat value="#{goalBean.goalList}" var="item" rowKeyVar="row">
			<tr id="<h:outputText value="#{item.id}" />" ondblclick="openGoal($(this));">
				<td class="sn"><h:outputText value="#{row + 1}" /></td>
				<td><span class="auto-link"><a href="#" onclick="openGoal($(this).closest('tr'));return false;"><h:outputText value="#{item.name}" /></a></span></td>
				<td><h:outputText value="#{item.startDate}" converter="DateConverter" /></td>
				<td><h:outputText value="#{item.endDate}" converter="DateConverter" /></td>
				<td><h:outputText value="#{item.type}" converter="GoalTypeConverter" /></td>
				<td><h:outputText value="#{item.priority}" converter="GoalPrioConverter" /></td>
				<td><h:outputText value="#{item.createTime}" converter="TimeConverter" /></td>
				<td><h:outputText value="#{item.modifyTime}" converter="TimeConverter" /></td>
			</tr>
			</a4j:repeat>
		</table>
		
		<input type="hidden" id="goalId" name="goalId" />
		<a4j:commandLink id="refresh" reRender="operate" oncomplete="initTab();" />
		<a4j:commandLink id="remove" action="#{goalBean.removeGoal}" oncomplete="refresh();" />
	</h:form>
	
	<div id="menu"></div>
	
	<script>
		var tableMain;
		initTab();
		
		$("#menu").omMenu({ 
			contextMenu: true,
			dataSource: [{id: "1", label: "编辑", action: "edit"}, {id: "2", label: "删除", action: "del"}],
			onSelect: function(item, e) {
				if(item.action == "del") {
					cloudConfirm(function() {
						$("#goalId").val(tableMain.table("getRow").attr("id"));
						$("#operate\\:remove").click();
					});
				}
				
				if(item.action == "edit") {
					buildPage("goalFrm", basePath + "pages/goal/goalAdd.jsf?goalId=" + tableMain.table("getRow").attr("id"));
				}
			}
		});
	
		function initTab() {
			tableMain = $("#goalTab").table({ menu: "menu" });			
		}
	
		function addGoal() {
			buildPage("goalFrm", basePath + "pages/goal/goalAdd.jsf");
		}
		
		function openGoal($tr) {
			buildPage("goalFrm", basePath + "pages/goal/goalTab.jsf?goalId=" + $tr.attr("id"));
		}
		
		function refresh() {
			$("#operate\\:refresh").click();
		}
	</script>
</body>
</html>
</f:view>