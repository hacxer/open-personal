<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/omui/themes/apusic/om-all.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/jquery-ui.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/colorpicker.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/cloud.ui.table.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/cloud.ui.select.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-ui.min.js" />"></script>
	
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/json2.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-core.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-menu.js" />"></script>
	<script type="text/javascript" src="<c:url value="/omui/ui/om-calendar.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/colorpicker.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/cloud.ui.select.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/plugin/cloud.ui.knowledgetable.js" />"></script>
</head>

<body>
	<div class="menu-title">
		<a href="#" onclick="clearPage('kngFrm', true);return false;">返回</a>
	</div>
	
	<div style="padding: 5px;">
		<div style="float: left;">
			<input id="saveBtn" type="button" class="button" value="保存" disabled="true" onclick="save();" />
			<input type="button" class="button" value="新增知识分类" onclick="tableMain.table('addKnowledge');" />
			<input type="button" class="button" value="新增节点" onclick="tableMain.table('addKnowledge', true);" />
			<input type="button" class="button" value="升级" onclick="tableMain.table('levelUp');" />
			<input type="button" class="button" value="降级" onclick="tableMain.table('levelDown');" />
		</div>
		
		<div style="float: left;padding-left: 18px;padding-bottom: 3px;margin-top: -3px;;">
			<input id="colorBtn" type="text" value="颜色" style="width: 80px;color: #999;" />
		</div>
	</div>
	
	<table id="knowledgeTab" class="list-table" cellpadding="0" cellspacing="0" style="border-top: 1px solid #999;clear: both;">
		<tr>
			<th width="50px">序号</th>
			<th width="500px">名称</th>
		</tr>
		
		<a4j:repeat value="#{knowledgeBean.knowledges}" var="item" rowKeyVar="row">
		<tr id="<h:outputText value="#{item.id}" />" level="<h:outputText value="#{item.level}" />" 
			isNode="<h:outputText value="#{item.isNode}" />"
			color="<h:outputText value="#{item.color}" />"
			style="background-color: <h:outputText value="#{item.color}" />;">
			
			<td class="sn"><h:outputText value="#{row + 1}" /></td>
			
			<h:panelGroup rendered="#{item.isNode == 'Y'}">
				<td><div style="font-weight: bold;padding-left: <h:outputText value="#{item.pad}" />px;"><h:outputText value="#{item.name}" /></div></td>
			</h:panelGroup>
			
			<h:panelGroup rendered="#{item.isNode == 'N'}">
				<td><div style="padding-left: <h:outputText value="#{item.pad}" />px;"><h:outputText value="#{item.name}" /></div></td>
			</h:panelGroup>
		</tr>
		</a4j:repeat>
	</table>
	
	<div id="menu"></div>
	
	<h:form id="operate">
		<input type="hidden" id="_info" name="knowledgeInfo" />
		
		<a4j:commandLink id="save" action="#{knowledgeBean.save}" />
	</h:form>
	
	<script>
		var tableMain;
		initTab();
		
		$("#colorBtn").colorpicker().on("change.color", function(event, color) {
			tableMain.table("color", color);
		});
		
		$("#menu").omMenu({ 
			contextMenu: true,
			
			dataSource: [{id: "1", label: "打开", action: "open", seperator: true}, 
			             {id: "2", label: "删除", action: "del"}],
			             
			onSelect: function(item, e) {
				if(item.action == "open") {
					
				}
				
				if(item.action == "del") {
					var $tr = tableMain.table("getRow"), id = $tr.attr("id");
					
					if(!id) { $tr.remove(); }
					else { $tr.attr("isDel", "Y").hide(); }
					
					$("#saveBtn").attr("disabled", false);
				}
			}
		});
		
		function initTab() {
			tableMain = $("#knowledgeTab").table({ menu: "menu" });			
		}
		
		function save() {
			if($("#saveBtn").attr("disabled"))  return;
				
			var datas = [], data;
			
			$("#knowledgeTab tr[isDel='Y']").each(function() {
				data = {i: $(this).attr("id")};
				datas.push(data);
			});
			
			$("#knowledgeTab tr[isEdit='Y']").each(function() {
				var $tr = $(this), $tds = $("td[isEdit='Y']", $tr), data = {};
				
				data.i = $tr.attr("id") ? $tr.attr("id") : "";
				data.nd = $tr.attr("isNode") == "Y" ? "Y" : "N";
				data.pi = $tr.attr("parentId") ? $tr.attr("parentId") : "";
				data.c = $tr.attr("color") ? $tr.attr("color") : "";
				
				$tds.each(function() {
					var $t = $(this), $d = $("div", $t), v = $d.text().trim();
					data.n = v;
				});
				
				datas.push(data);
			});
			
			$("#saveBtn").attr("disabled", true);
			$("#_info").val(JSON.stringify(datas));
			$("#operate\\:save").click();
		}
		
		function refresh(info) {
			if(!info)  return;
			var $tr = $("#" + info.i);
			
			$("td:eq(2) div", $tr).text(info.n);
			$("td:eq(3) div", $tr).attr("val", info.p).text(info.pn);
			$("td:eq(4) div", $tr).text(info.s);
			$("td:eq(5) div", $tr).text(info.e);
		}
	</script>
</body>
</html>
</f:view>