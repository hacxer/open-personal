<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	
	<style>
		.box {
			margin: 10px;border: 1px solid #999;border-radius: 10px;
		}
		
		.box .box-head {
			height: 25px;background-color: #fbd5b5;font-weight: bold;border-radius: 10px 10px 0 0;padding: 8px 0 0 15px;
		}
		
		.box .box-body {
			padding: 10px 15px 15px 15px;line-height: 30px;
		}
		
		.box .box-body span {
			display: inline-block;min-width: 133px;
		}
		
		.box .box-body a {
			color: black;text-decoration: none;
		}
		
		.box .box-body a:hover {
			text-decoration: underline;
		}
	</style>
</head>

<body>
	<div class="menu-title">
		<a href="#" onclick="addCatagory();return false;">新增分类</a>
		<a href="#" onclick="addSite();return false;">新增网址</a>
	</div>
	
	<div id="outerDiv" style="overflow: auto;">
	<h:form id="operate">
		<a4j:repeat value="#{siteBean.sites}" var="item">
			<div class="box">
				<div class="box-head"><h:outputText value="#{item.catagory.name}" /></div>
				<div class="box-body">
					<a4j:repeat value="#{item.sites}" var="site">
						<span><a href="<h:outputText value="#{site.url}" />" target="_blank"><h:outputText value="#{site.name}" /></a></span>
					</a4j:repeat>
				</div>
			</div>
		</a4j:repeat>
		
		<a4j:commandLink id="refresh" reRender="operate" />
	</h:form>
	</div>
	
	<script>
		$("#outerDiv").height(getViewH());
		
		function addCatagory() {
			buildPage("catFrm", basePath + "pages/site/siteCatagoryAdd.jsf");
		}
		
		function addSite() {
			buildPage("siteFrm", basePath + "pages/site/siteAdd.jsf");
		}
		
		function refresh() {
			$("#operate\\:refresh").click();
		}
	</script>
</body>
</html>
</f:view>