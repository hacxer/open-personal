<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
</head>

<body>
	<div class="menu-title">
		<a href="#" onclick="saveSign();return false;">保存</a>
	</div>
	
	<h:form id="operate">
		<table class="edit-table">
			<tr>
				<td class="edit-label">个性签名</td>
				<td class="edit-whole" colspan="3"><h:inputText id="sign" value="#{userBean.sign}" /></td>
			</tr>
		</table>
		
		<a4j:commandLink id="save" action="#{userBean.saveSign}" oncomplete="alert('修改个性签名成功！');" />
	</h:form>
	
	<script>
		function saveSign() {
			$("#operate\\:save").click();
		}
	</script>
</body>
</html>
</f:view>