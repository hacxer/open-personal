var basePath = "/open-personal/";

function buildPage(id, url, isTop) {
	var iframe = "<iframe id='" + id + "' src='" + url + "'";
	iframe += " style='position: absolute;left: 0;top: 0;background-color: white;'";
	iframe += " width='100%' height='100%' frameborder='0' />";
	
	if(isTop) {
		$(parent.document.body).append(iframe);
	} else {
		$(document.body).append(iframe);
	}
}

function clearPage(id, isRefresh) {
	if(isRefresh) {
		parent.refresh();
	}
	
	$("#" + id, parent.document).remove();
}

function cloudConfirm(func) {
	$.omMessageBox.confirm({
        title: $("#delTle", top.document).text(),
        content: $("#delTip", top.document).text(),
        onClose: function(value) {
            if(value) { func(); }
        }
    });
}

function getViewH() {
	return 567;
}

function getViewW() {
	return 1155;
}

/**
 * ****************************************************************************
 * date operates
 * ****************************************************************************
 */
function getDateStr(date) {
	if(!date)  return "";
	
	return date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate()
			+ " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
}