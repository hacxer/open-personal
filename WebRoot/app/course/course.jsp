<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/app.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/plugin/cloud.ui.table.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	
	<style>
		body {margin: 0; padding: 0;background-image: none;background-color: white;}
		th {width: 120px;border-top: 1px solid #999!important;}
		.time {text-align: center;background-color: rgb(219, 226, 245);}
	</style>
</head>

<body>
	<div class="menu-title">
		<div class="app-title">课程表</div>
		<div class="app-close" onclick="clearPage('appFrm');"><img src="<c:url value="/images/close.png" />" /></div>
	</div>
	
	<div id="outerDiv" style="overflow: auto;">
		<div id="courseDiv">
			<table class="list-table" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="8" style="border: 0;"><input type="button" onclick="editCourse();" class="button" value="编辑" /></td>
				</tr>
				<tr>
					<th style="width: 80px;"></th><th>周一</th><th>周二</th><th>周三</th><th>周四</th><th>周五</th><th>周六</th><th>周日</th>
				</tr>
				<tr class="time">
					<td colspan="8">上午</td>
				</tr>
				<tr>
					<td align="center" style="padding-left: 0;">第一节</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				</tr>
				<tr>
					<td align="center" style="padding-left: 0;">第二节</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				</tr>
				<tr>
					<td align="center" style="padding-left: 0;">第三节</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				</tr>
				<tr>
					<td align="center" style="padding-left: 0;">第四节</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				</tr>
				<tr class="time">
					<td colspan="8">下午</td>
				</tr>
				<tr>
					<td align="center" style="padding-left: 0;">第一节</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				</tr>
				<tr>
					<td align="center" style="padding-left: 0;">第二节</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				</tr>
				<tr>
					<td align="center" style="padding-left: 0;">第三节</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				</tr>
				<tr>
					<td align="center" style="padding-left: 0;">第四节</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				</tr>
				<tr class="time">
					<td colspan="8">晚上</td>
				</tr>
				<tr>
					<td align="center" style="padding-left: 0;">第一节</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				</tr>
				<tr>
					<td align="center" style="padding-left: 0;">第二节</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				</tr>
				<tr>
					<td align="center" style="padding-left: 0;">第三节</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				</tr>
			</table>
		</div>
	</div>
	
	<h:inputHidden id="info" value="#{appBean.courseInfo}" />
	
	<script>
		$("#courseDiv").css({"margin-left": getViewW() / 2 - 460, "margin-top": getViewH() / 2 - 245});
		
		function editCourse() {
			buildPage("courseFrm", basePath + "app/course/courseEdit.jsf");
		}
		
		function refresh() {
			window.location = window.location;
		}
		
		// init course table
		initCourseTable();
		
		function initCourseTable() {
			var info = $("#info").val();
			if(!info)  return;
			
			var datas = eval(info), $rows = $("table.list-table tr:gt(1):not(.time)"), $r, $cs;
			
			for(var i in datas) {
				var row = datas[i], $r = $rows.eq(i), $cs = $("td:gt(0)", $r);
				
				for(var j in row) {
					$cs.eq(j).text(row[j]);
				}
			}
		}
	</script>
</body>
</html>
</f:view>