<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib prefix="f" uri="http://java.sun.com/jsf/core" %>
<%@ taglib prefix="h" uri="http://java.sun.com/jsf/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="a4j" uri="http://richfaces.org/a4j" %>

<f:view>
<html>
<head>
	<title>Cloud Manage</title>
	
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/cloud.css" />">
	<link rel="stylesheet" type="text/css" href="<c:url value="/css/app.css" />">
	
	<script type="text/javascript" src="<c:url value="/scripts/jquery/jquery-1.7.1.min.js" />"></script>
	<script type="text/javascript" src="<c:url value="/scripts/cloud.js" />"></script>
	
	<style type="text/css">
		body {margin: 0; padding: 0;background-image: none;background-color: white;}
		
        * {
        	margin: 0;
        	padding: 0;
        }
        
        #clock {
        	position: relative;
        	width: 600px;
        	height: 600px;
        	margin: 20px auto 0 auto;
        	background: url(clockface.jpg);
        	list-style: none;
        	}
        
        #sec, #min, #hour {
        	position: absolute;
        	width: 30px;
        	height: 600px;
        	top: 0px;
        	left: 285px;
        	}
        
        #sec {
        	background: url(sechand.png);
        	z-index: 3;
           	}
           
        #min {
        	background: url(minhand.png);
        	z-index: 2;
           	}
           
        #hour {
        	background: url(hourhand.png);
        	z-index: 1;
           	}
           	
        p {
            text-align: center; 
            padding: 10px 0 0 0;
            }
    </style>
</head>

<body style="overflow: hidden;">
	<div class="menu-title">
		<div class="app-title">CSS3时钟</div>
		<div class="app-close" onclick="clearPage('appFrm');"><img src="<c:url value="/images/close.png" />" /></div>
	</div>
	
	<div id="outerDiv">
		<ul id="clock" style="margin-top: 0;">	
		   	<li id="sec"></li>
		   	<li id="hour"></li>
			<li id="min"></li>
		</ul>
	</div>
	
	<script>
		$(document).ready(function() {
	        setInterval( function() {
	        var seconds = new Date().getSeconds();
	        var sdegree = seconds * 6;
	        var srotate = "rotate(" + sdegree + "deg)";
	        
	        $("#sec").css({"-moz-transform" : srotate, "-webkit-transform" : srotate});
	            
	        }, 1000 );
	        
	   
	        setInterval( function() {
	        var hours = new Date().getHours();
	        var mins = new Date().getMinutes();
	        var hdegree = hours * 30 + (mins / 2);
	        var hrotate = "rotate(" + hdegree + "deg)";
	        
	        $("#hour").css({"-moz-transform" : hrotate, "-webkit-transform" : hrotate});
	            
	        }, 1000 );
	  
	  
	        setInterval( function() {
	        var mins = new Date().getMinutes();
	        var mdegree = mins * 6;
	        var mrotate = "rotate(" + mdegree + "deg)";
	        
	        $("#min").css({"-moz-transform" : mrotate, "-webkit-transform" : mrotate});
	            
	        }, 1000 );
	    });
	</script>
</body>
</html>
</f:view>