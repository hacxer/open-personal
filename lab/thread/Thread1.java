package thread;

public class Thread1 implements Runnable {
	
	private SynClass synClass;
	
	public Thread1(SynClass synClass) {
		this.synClass = synClass;
	}

	public void run() {
		
		System.out.println("Thread 1.....");
		synClass.staticPrint();
	}
}
