package thread;

public class Thread2 implements Runnable {
	
	private SynClass synClass;
	
	public Thread2(SynClass synClass) {
		this.synClass = synClass;
	}

	public void run() {
		
		System.out.println("Thread 2.....");
		synClass.staticPrint();
	}
}
