package struts;

import com.opensymphony.xwork2.Action;

public class StrutsAction implements Action {
	
	private String username;

	public String execute() throws Exception {
		
		System.out.println("in struts action............");
		System.out.println("web page param: " + username);
		
		return "success";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
